$(document).ready(function () {

    var flag1 = 0,
        flag2 = 0;

    $('#logInPopUpBtn').click(function () {
        $('#logIn').modal();
    });

    $('#signUpPopUpBtn').click(function () {
        $('#logIn').modal('hide');
        $('#signUp').modal();
    });

    $('.input').on('change paste keyup', function () {
        flag1 = 0;
        $('.input').each(function () {
            if (/[1234567890~` !#$%\^&*+=\)\(\-\[\]\\';_,.@/{}|\\":<>\?]/g.test($(this).val()))
                flag1 = 1;
        });
        if (flag1)
            $('#msg').html('Only alphabets please !');
        else
            $('#msg').html('&nbsp;');
    });

    $('.input2').on('change paste keyup', function () {
        flag2 = 0;
        $('.input2').each(function () {
            if (/[1234567890~` !#$%\^&*+=\)\(\-\[\]\\';_,.@/{}|\\":<>\?]/g.test($(this).val()))
                if (!($('input[name=individuality]:checked').val() == 'Individual' && $(this).context.id == 'organisation'))
                    flag2 = 1;
        });
        if (flag2)
            $('#msg2').html('Only alphabets please !');
        else
            $('#msg2').html('&nbsp;');
    });

    $('#logInBtn').on('click keyup', function () {
        flag1 = 0;
        $('.input').each(function () {
            if (/[1234567890~` !#$%\^&*+=\)\(\-\[\]\\';_,.@/{}|\\":<>\?]/g.test($(this).val()) || $(this).val() == '')
                flag1 = 1;
        });
        if (flag1)
            $('#msg').html('Fields cannot be empty or contain invalid characters');
        else
            $.post('/login', {
                username: $('#username').val(),
                password: $('#password').val()
            });
    });

    $('#signUpBtn').on('click keyup', function () {
        flag2 = 0;
        $('.input2').each(function () {
            if (/[1234567890~` !#$%\^&*+=\)\(\-\[\]\\';_,.@/{}|\\":<>\?]/g.test($(this).val()) || $(this).val() == '')
                if (!($('input[name=individuality]:checked').val() == 'Individual' && $(this).context.id == 'organisation'))
                    flag2 = 1;
        });
        if (flag2)
            $('#msg2').html('Fields cannot be empty or contain invalid characters');
        else if ($('#newPassword').val() != $('#confPassword').val())
            $('#msg2').html('Password mismatch !');
        else
            $.post('/signup', {
                first_name: $('#firstname').val(),
                last_name: $('#lastname').val(),
                email_id: $('#emailId').val(),
                type_of_user: $('input[name=individuality]:checked').val(),
                organisation: $('#organisation').val(),
                password: $('#newPassword').val()
            });
    });

});

// /[1234567890~`!#$%\^&*+=\)\(\-\[\]\\';_,.@/{}|\\":<>\?]/g.test();
