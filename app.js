var express = require('express');
var app = express();

var mysql = require("mysql");

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "nfsmw",
    database: "NIWeek"
});

con.connect(function (err) {
    if (err) {
        console.log('Error connecting to Db\n');
        return;
    } else {
        console.log('Connection established\n');
    }
});

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/Home.html');
});

app.get('/bootstrap.css', function (req, res) {
    res.sendFile(__dirname + '/bootstrap.css');
});

app.get('/bootstrap.js', function (req, res) {
    res.sendFile(__dirname + '/bootstrap.js');
});

app.get('/jquery-2.2.4.js', function (req, res) {
    res.sendFile(__dirname + '/jquery-2.2.4.js');
});

app.get('/Introduce.jpg', function (req, res) {
    res.sendFile(__dirname + '/Introduce.jpg');
});

app.get('/WhoAreYou.png', function (req, res) {
    res.sendFile(__dirname + '/WhoAreYou.png');
});

app.get('/Home.js', function (req, res) {
    res.sendFile(__dirname + '/Home.js');
});

app.get('/Home.css', function (req, res) {
    res.sendFile(__dirname + '/Home.css');
});

app.get('/check/:username/:password', function (req, res) {
    con.query('SELECT * FROM Users WHERE BINARY username = ?', req.params.username, function (err, rows) {
        if (err) console.log(err);
        else {
            if (rows.length)
                res.json({
                    flag: 'false'
                });
            else {
                con.query('INSERT INTO Users SET ?', {
                    username: req.params.username,
                    password: req.params.password
                }, function (err, result) {
                    if (err) console.log('Error ! ' + err);
                    else {
                        console.log('Result : ' + result);
                        res.json({
                            flag: 'true'
                        });
                    }
                });
            }
        }
    });
});

app.get('/logIn/:username/:password', function (req, res) {
    con.query('SELECT * FROM Users WHERE BINARY username = ?', req.params.username, function (err, rows) {
        if (err) console.log(err);
        else {
            if (rows.length == 0)
                res.json({
                    flag: 'false'
                });
            else {
                if (req.params.password == rows[0].password) {
                    console.log(result);
                    res.json({
                        flag: 'true'
                    });
                } else {
                    console.log("Invalid Password");
                    res.json({
                        flag: 'false'
                    });
                }
            }
        }
    });
});

var port = 8000;

app.listen(port, function () {
    console.log('Listening to ' + port);
});
